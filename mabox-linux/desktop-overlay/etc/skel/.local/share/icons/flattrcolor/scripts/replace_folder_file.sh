#!/usr/bin/env bash
#	default color: 178984
oldglyph=#182838
newglyph=#0d1a19

#	Front
#	default color: 36d7b7
oldfront=#315071
newfront=#214542

#	Back
#	default color: 1ba39c
oldback=#22374d
newback=#172f2d

sed -i "s/#524954/$oldglyph/g" $1
sed -i "s/#9b8aa0/$oldfront/g" $1
sed -i "s/#716475/$oldback/g" $1
sed -i "s/$oldglyph;/$newglyph;/g" $1
sed -i "s/$oldfront;/$newfront;/g" $1
sed -i "s/$oldback;/$newback;/g" $1
