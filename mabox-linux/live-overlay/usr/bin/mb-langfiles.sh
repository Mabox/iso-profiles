#!/bin/bash
#
#
#
# Dodać kopiowanie menu prepend.csv i conky bez ilości aktualizacji
# oraz obrazków dla instalatora PL i ES
#

case $LANG in
    pl*)
        rsync -a /usr/share/mabox/lang/pl/ /home/mabox/
        sudo rsync -a /etc/calamares/branding/pl/instalator/ /etc/calamares/branding/manjaro/
        rsync -a /etc/calamares/branding/pl/.config/ /home/mabox/.config/
        ;;
    es*)
        rsync -a /usr/share/mabox/lang/es/ /home/mabox/
        sudo rsync -a /etc/calamares/branding/es/instalator/ /etc/calamares/branding/manjaro/
        rsync -a /etc/calamares/branding/es/.config/ /home/mabox/.config/
        ;;
    *)
    exit 0
    ;;
esac
